﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;

namespace YMLCleanup
{
	class Program
	{
		static void Main(string[] args)
		{

			using (StreamReader r = new StreamReader("c:\\temp\\mytab.yml"))
			{
				string yml = r.ReadToEnd();

				var cleanedYML = ApplyCleanProcessOnYaml(yml);
			}
				Console.WriteLine("Hello World!");
		}

		public static string ApplyCleanProcessOnYaml(string yamlInput)
		{

			// convert string/file to YAML object
			var r = new StringReader(yamlInput);
			var deserializer = new YamlDotNet.Serialization.Deserializer();
			var yamlObject = deserializer.Deserialize(r);

			// converting yaml to json for clean up
			JsonSerializer js = new Newtonsoft.Json.JsonSerializer();
			var w = new StringWriter();
			js.Serialize(w, yamlObject);
			string jsonText = w.ToString();

			//removing IDs elements from FormElements
			var parent = JsonConvert.DeserializeObject<JObject>(jsonText);

			var tab = (JArray)parent.Property("Tab_Groups").Value;

			foreach (var item in tab)
			{
				JObject group = item as JObject;

				if (group.Property("Group_FormElements") != null)
				{
					((JArray)group.Property("Group_FormElements").Value)
					.Select(jo => (JObject)jo)
					.ToList()
					.ForEach(x =>
						x
							.Properties()
							.ToList()
							.ForEach(p =>
							{
								if (p.Name == "FormElementId" || p.Name == "Guid" || p.Name == "GroupId" || p.Name == "GroupGuid")
									p.Remove();
							}));

					group.Property("GroupId").Remove();
					group.Property("TabId").Remove();
				}
				else // removing groups if there are not having form elements
				{
					group.RemoveAll();
				}


			}


			//Removing from tab level node
			parent.Property("TabId").Remove();
			parent.Property("Guid").Remove();
			parent.Property("FormId").Remove();

			//converting back to yaml
			var expConverter = new Newtonsoft.Json.Converters.ExpandoObjectConverter();
			dynamic deserializedObject = JsonConvert.DeserializeObject<System.Dynamic.ExpandoObject>(parent.ToString(), expConverter);
			var serializer = new YamlDotNet.Serialization.Serializer();
			string yaml = serializer.Serialize(deserializedObject);

			//using (StreamWriter streamWriter = new StreamWriter(@"C:\Temp\ContactFormElementsCleaned.yml"))
			//{
			//	streamWriter.Write(yaml);
			//}

			return yaml;

		}
	}
}
